import { HTMLAttributes, ReactNode } from "react";

export default function AutoPlayVideo({children, className, ...props}: { children?: ReactNode } & HTMLAttributes<HTMLVideoElement>) {
    return (
        <video
            className={`${className || ''} object-fill`}
            muted
            loop
            autoPlay
            playsInline
            webkit-playsinline="true"
            mtt-playsinline="true"
            preload="auto"
            x5-video-player-type="h5-page"
            {...props}
        >
            {children}
        </video>
    )
}
