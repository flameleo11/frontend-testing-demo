import { DraggablePanel as _DraggablePanel, DraggablePanelProps as _DraggablePanelProps } from '@ant-design/pro-editor';
import { cloneElement, HTMLAttributes, MouseEventHandler, ReactElement, useRef, useState } from "react";
import "./index.scss";

type DraggablePanelProps = {
    left: ReactElement;
    right: ReactElement;
    AntdDraggablePanelProps?: Omit<_DraggablePanelProps, 'minWidth'>;
} & HTMLAttributes<HTMLDivElement>

export default function DraggablePanel({ left, right, AntdDraggablePanelProps, className, ...attrs }: DraggablePanelProps) {
    const containerRef = useRef<HTMLDivElement | null>(null)
    const [width, setWidth] = useState('100px')
    const updatePositionHandle: MouseEventHandler = ({ nativeEvent }) => {
        // 容器宽度 - 光标相对容器 x 偏移量 + 容器相对页面 x 偏移量
        setWidth(() => `${(containerRef.current?.clientWidth || 0) - nativeEvent.clientX + (containerRef.current?.offsetLeft || 0)}px`)
    }

    return (
        <div className={`flex overflow-hidden ${className}`} style={{ width }}  ref={containerRef} onMouseMove={updatePositionHandle} {...attrs}>
            <div className="flex-1">
                {
                    cloneElement(left, { style: { height: '100%', width: '100%'}, })
                }
            </div>
            <div style={{ width }}>
                <_DraggablePanel
                    className="draggable-panel"
                    expandable={false}
                    size={{ width, height: '100%' }}
                    minWidth={0}
                    onSizeDragging={(_, ui) => setWidth(() => ui?.width as string)}
                    {...AntdDraggablePanelProps}>
                    <div className="draggable-panel__handle">
                        <div className="draggable-panel__handle__left-arrow" />
                        <div className="draggable-panel__handle__right-arrow" />
                    </div>
                    {
                        cloneElement(right, { style: { height: '100%', width: '100%' }})
                    }
                </_DraggablePanel>
            </div>
        </div>
    )
}
