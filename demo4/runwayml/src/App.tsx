import { Button, Space } from 'antd'
import { ArrowUpOutlined, PlayCircleOutlined } from '@ant-design/icons'
import DraggablePanel from '@/component/DraggablePanel';
import AutoPlayVideo from "@/component/AutoPlayVideo.tsx";
import DiscordSVG from '@/assets/discord.svg'
import "./App.scss"

function App() {
    return (
        <div className="text-white overflow-x-hidden">
            <AutoPlayVideo className="absolute top-0 left-0 w-full h-screen">
                <source type="video/webm" src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/gen2_short_header.webm"/>
                <source type="video/mp4" src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/gen2_short_header.mp4"/>
            </AutoPlayVideo>
            <div className="z-10 relative">
                <header
                    className="box-border flex items-center justify-between absolute top-0 left-0 w-full max-sm:px-6 lg:px-12 pb-6 pt-7 mx-auto">
                    <div className="flex relative  hover:opacity-70">
                        <svg width="91" className="text-white" viewBox="0 0 122 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M33.1792 5.25873H36.5453L36.8608 7.01925C37.5194 5.62608 38.9373 4.94336 40.4349 4.94336C41.1698 4.94336 41.9082 5.12704 42.4074 5.33844L41.617 9.41052C41.0901 9.09515 40.4349 8.88375 39.6445 8.88375C38.2786 8.88375 37.1728 9.8056 37.1728 11.8538V18.3968H33.1792V5.25873Z"
                                className="fill-current"></path>
                            <path
                                d="M43.9363 13.5904V5.25879H47.9333V12.8279C47.9333 14.3528 48.8 15.1673 50.0618 15.1673C51.3514 15.1673 52.4538 14.1692 52.4538 11.9615V5.25879H56.4509V18.4008H53.1125L52.6653 16.6679C52.0344 17.7181 50.457 18.7162 48.5122 18.7162C45.9053 18.7162 43.9363 17.0318 43.9363 13.5904Z"
                                className="fill-current"></path>
                            <path
                                d="M58.6697 5.25873H62.0081L62.4553 6.99153C63.0862 5.94145 64.6358 4.94336 66.5841 4.94336C69.1598 4.94336 71.1046 6.59992 71.1046 10.0413V18.4003H67.1075V10.8834C67.1075 9.33428 66.3171 8.49213 65.0033 8.49213C63.7934 8.49213 62.6633 9.49023 62.6633 11.7498V18.4003H58.6663V5.25873H58.6697Z"
                                className="fill-current"></path>
                            <path
                                d="M71.7871 5.25879H75.8361L78.2281 14.1692L80.3567 8.30862L79.3063 5.25879H83.3276L85.931 14.1692L88.2433 5.25879H92.3166L88.0041 18.4008H83.851L82.0657 13.171L80.2249 18.4008H76.0961L71.7871 5.25879Z"
                                className="fill-current"></path>
                            <path
                                d="M92.3926 11.8304C92.3926 7.28296 95.6512 4.94336 98.6499 4.94336C100.491 4.94336 101.805 5.67817 102.46 6.4407L102.827 5.25877H106.533V18.4021H103.011L102.46 17.1162C101.909 17.7713 100.726 18.7175 98.4627 18.7175C95.2318 18.7175 92.3926 16.0348 92.3926 11.8304ZM102.619 11.8304C102.619 9.70227 101.253 8.38863 99.5165 8.38863C97.7555 8.38863 96.4139 9.73 96.4139 11.8304C96.4139 13.9309 97.7555 15.2757 99.5165 15.2757C101.253 15.2722 102.619 13.9586 102.619 11.8304Z"
                                className="fill-current"></path>
                            <path
                                d="M110.738 23.6539L112.947 17.6897L107.716 5.25879H112L114.919 13.3786L117.547 5.25879H121.728L114.815 23.6539H110.738Z"
                                className="fill-current"></path>
                            <path
                                d="M18.2068 23.7621C15.3295 24.0255 12.9236 20.6498 11.0794 18.9378C10.1469 25.6648 -0.00692973 25.0271 3.54902e-06 18.203C0.00347019 15.3369 3.54902e-06 8.33263 3.54902e-06 5.55657C3.54902e-06 4.55844 0.273868 3.55338 0.786931 2.70081C1.76106 1.04765 3.64344 -0.0197939 5.5605 0.00100047C8.44821 0.00446621 15.4023 -0.00246526 18.2068 0.00100047C25.0292 0.00100047 25.6774 10.1625 18.9348 11.0775L22.1345 14.2764C25.6497 17.5861 23.0116 23.9076 18.2068 23.7621ZM16.6572 19.7592C18.6401 21.8039 21.8052 18.6363 19.7633 16.6539L14.2167 11.1087H11.1141C11.1141 11.4102 11.1141 13.9956 11.1141 14.214L15.9084 19.0071L16.6572 19.7592ZM3.35918 18.2065C3.31411 21.0484 7.79648 21.0588 7.75141 18.2065V5.55657C7.78955 4.13909 6.29196 3.01272 4.93997 3.44941C4.88104 3.46674 4.82557 3.48406 4.77357 3.50486C3.92424 3.82024 3.33491 4.69014 3.35918 5.59816V18.2065ZM18.2068 7.75038C21.0564 7.79544 21.0529 3.31424 18.2068 3.3593H10.6738C11.2458 4.56537 11.1002 6.4438 11.1106 7.75038C11.5197 7.75038 18.0057 7.75038 18.2068 7.75038Z"
                                className="fill-current"></path>
                        </svg>
                        <svg className="ml-1 text-white" width="78" viewBox="0 0 105 19" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M9.828 9.854C11.544 9.048 12.688 7.618 12.688 5.616C12.688 2.444 10.478 0.285999 6.89 0.285999H0V18.486H2.21V11.076H7.15C8.866 11.076 10.01 12.324 10.114 14.092L10.426 18.486H12.558L12.246 13.962C12.116 12.116 11.258 10.634 9.828 9.854ZM2.21 2.262H6.89C9.438 2.262 10.4 3.822 10.4 5.616C10.4 7.41 9.438 9.074 6.89 9.074H2.21V2.262Z"
                                className="fill-current"></path>
                            <path
                                d="M25.6112 14.274C25.1952 15.912 23.8432 16.848 21.7112 16.848C19.2412 16.848 17.4472 14.924 17.2912 12.22H27.5352C27.5612 12.038 27.5872 11.596 27.5872 11.31C27.5872 7.826 25.2992 5.174 21.4772 5.174C17.6292 5.174 15.2372 8.008 15.2372 11.856C15.2372 15.6 17.8372 18.694 21.7112 18.694C24.9612 18.694 27.2752 16.952 27.7172 14.274H25.6112ZM21.4772 6.942C23.8172 6.942 25.2992 8.346 25.4552 10.556H17.3952C17.7852 8.242 19.2672 6.942 21.4772 6.942Z"
                                className="fill-current"></path>
                            <path
                                d="M34.8107 18.694C37.6968 18.694 39.6208 17.186 39.6208 14.846C39.6208 8.84 31.9508 12.35 31.9508 8.814C31.9508 7.67 32.9647 6.942 34.4987 6.942C35.6947 6.942 37.3328 7.514 37.5148 9.178H39.5428C39.3868 6.76 37.4368 5.174 34.5508 5.174C31.7948 5.174 29.9228 6.63 29.9228 8.84C29.9228 14.508 37.5408 10.998 37.5408 14.898C37.5408 15.964 36.4487 16.874 34.8107 16.874C32.9128 16.874 31.7687 15.964 31.5868 14.274H29.5848C29.7928 17.004 31.7687 18.694 34.8107 18.694Z"
                                className="fill-current"></path>
                            <path
                                d="M51.9921 14.274C51.5761 15.912 50.2241 16.848 48.0921 16.848C45.6221 16.848 43.8281 14.924 43.6721 12.22H53.9161C53.9421 12.038 53.9681 11.596 53.9681 11.31C53.9681 7.826 51.6801 5.174 47.8581 5.174C44.0101 5.174 41.6181 8.008 41.6181 11.856C41.6181 15.6 44.2181 18.694 48.0921 18.694C51.3421 18.694 53.6561 16.952 54.0981 14.274H51.9921ZM47.8581 6.942C50.1981 6.942 51.6801 8.346 51.8361 10.556H43.7761C44.1661 8.242 45.6481 6.942 47.8581 6.942Z"
                                className="fill-current"></path>
                            <path
                                d="M61.7116 5.174C58.7736 5.174 56.3816 7.046 56.1476 9.568H58.1236C58.2276 8.138 59.7356 6.942 61.6596 6.942C63.8956 6.942 65.1696 8.164 65.1696 9.958C65.1696 10.374 64.9096 10.66 64.3376 10.66H61.3736C58.2016 10.66 56.1476 12.246 56.1476 14.742C56.1476 17.134 57.9416 18.694 60.6976 18.694C62.7256 18.694 64.4676 17.732 65.1696 16.224V18.486H67.2496V10.036C67.2496 7.124 65.1176 5.174 61.7116 5.174ZM61.0096 16.926C59.3456 16.926 58.2796 16.068 58.2796 14.716C58.2796 13.182 59.5536 12.246 61.5036 12.246H65.1696V12.584C65.1696 15.288 63.6356 16.926 61.0096 16.926Z"
                                className="fill-current"></path>
                            <path
                                d="M77.6332 7.228V5.382H75.9692C74.0452 5.382 72.9531 6.24 72.4072 8.034V5.382H70.3272V18.486H72.4072V12.714C72.4072 9.62 73.1612 7.228 75.8132 7.228H77.6332Z"
                                className="fill-current"></path>
                            <path
                                d="M84.8553 18.694C88.1833 18.694 90.3413 16.926 90.7833 14.248H88.7553C88.2873 15.834 86.8573 16.822 84.8813 16.822C82.0473 16.822 80.6433 14.612 80.6433 11.934C80.6433 9.256 82.0213 7.046 84.8813 7.046C86.8573 7.046 88.4953 8.216 88.7553 9.802H90.8093C90.4193 6.89 87.7933 5.174 84.8553 5.174C80.7213 5.174 78.4853 8.424 78.4853 11.934C78.4853 15.444 80.7213 18.694 84.8553 18.694Z"
                                className="fill-current"></path>
                            <path
                                d="M93.4604 18.486H95.5405V11.414C95.5405 8.866 97.1525 7.072 99.3885 7.072C101.364 7.072 102.664 8.58 102.664 10.816V18.486H104.744V10.478C104.744 7.28 102.82 5.174 99.8565 5.174C97.8545 5.174 96.2165 6.084 95.5405 7.904V0H93.4604V18.486Z"
                                className="fill-current"></path>
                        </svg>
                    </div>
                    <nav className="flex items-center text-sm">
                        <Space size="large">
                            <a href="#" className="text-white hover:text-white hover:underline">Publications</a>
                            <a href="#" className="text-white hover:text-white hover:underline">Gen-2</a>
                            <a href="#" className="text-white hover:text-white hover:underline">Gen-1</a>
                            <a href="#" className="text-white hover:text-white hover:underline">Careers</a>
                            <a href="#" className="text-white hover:text-white hover:underline">About</a>
                            <Button className="transition-none hover:!bg-black hover:!border-white hover:!text-white"
                                    shape="round" ghost>Sign in to Runway</Button>
                        </Space>
                    </nav>
                </header>
                <main>
                    <div className="w-screen h-screen flex flex-col text-center justify-center items-center text-white">
                        <h1 className="home__title mb-6 mx-auto xl:text-[5.4vw] lg:text-[7.6vw] max-sm:text-[8.4vw]">
                            Gen-2: The Next Step
                            <br></br>
                            Forward for Generative AI
                        </h1>
                        <h2 className="mb-10 text-xl lg:text-4xl">
                            A multimodal AI system that can generate novel videos with text, images or video clips.
                        </h2>
                        <div className="grid gap-3 justify-center items-center grid-flow-row lg:grid-flow-col">
                            <Button shape="round" size="large"
                                    className="w-max mx-auto hover:!text-black hover:!bg-white hover:!border-0"
                                    icon={<ArrowUpOutlined className="rotate-45"/>}>
                                Try Gen-2 in Runway
                            </Button>

                            <Button shape="round" size="large"
                                    className="w-max mx-auto hover:!text-black hover:!bg-white hover:!border-0"
                                    icon={<ArrowUpOutlined className="rotate-45"/>}>
                                Try Gen-2 for iOS
                            </Button>
                            <Button shape="round" size="large"
                                    className="w-max mx-auto hover:!text-black hover:!bg-white hover:!border-0"
                                    icon={<PlayCircleOutlined/>}>
                                Gen-2 Explained
                            </Button>
                            <Button shape="round" size="large"
                                    className="flex items-center justify-center w-max mx-auto hover:!text-black hover:!bg-white hover:!border-0"
                                    icon={<img src={DiscordSVG} alt="" width="16" height="16"/>}>
                                Join Discord
                            </Button>
                        </div>
                    </div>
                    <DraggablePanel
                        style={{
                            height: '500px'
                        }}
                        className="lg:mx-auto lg:w-10/12 lg:my-12 sm:w-full"
                        left={
                            <AutoPlayVideo className="!object-cover">
                                <source type="video/webm"
                                        src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/storyboard_input.webm"/>
                                <source type="video/mp4"
                                        src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/storyboard_input.mp4"/>
                            </AutoPlayVideo>
                        }
                        right={
                            <AutoPlayVideo className="!object-cover">
                                <source type="video/webm"
                                        src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/storyboard_generated-gen.webm"/>
                                <source type="video/mp4"
                                        src="https://11eavkocrk.oss-cn-chengdu.aliyuncs.com/storyboard_generated-gen.mp4"/>
                            </AutoPlayVideo>
                        }
                    >
                    </DraggablePanel>
                </main>
            </div>
        </div>
    )
}

export default App
